<footer class="sticky-footer">
    <div class="container my-auto">
        <div class="copyright text-center my-auto">
            <span>Copyright © My Speaker Leads {{date('Y')}}</span>
        </div>
    </div>
</footer>