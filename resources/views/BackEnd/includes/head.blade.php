
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">

<title>@yield('title') | My Speaker Leaders </title>

<link rel="icon" href="{{asset('BackEnd1/img/favicon.png')}}" type="image/png" >

<!-- Custom fonts for this template-->
<link href="{{asset('BackEnd1/vendor/fontawesome-free/css/all.min.css')}}" rel="stylesheet" type="text/css">

<!-- Page level plugin CSS-->
<link href="{{asset('BackEnd1/vendor/datatables/dataTables.bootstrap4.css')}}" rel="stylesheet">

<!-- Custom styles for this template-->
<link href="{{asset('BackEnd1/css/sb-admin.css')}}" rel="stylesheet">

<!-- Custom styles for csv upload page template-->
<link href="{{asset('BackEnd1/css/nikola-csv.css')}}" rel="stylesheet">
<link href="{{asset('BackEnd1/css/modal-loading.css')}}" rel="stylesheet">
<link href="{{asset('BackEnd1/css/modal-loading-animate.css')}}" rel="stylesheet">