<script src="{{asset('BackEnd1/vendor/jquery/jquery.min.js')}}"></script>
<script src="{{asset('BackEnd1/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

<!-- Core plugin JavaScript-->
<script src="{{asset('BackEnd1/vendor/jquery-easing/jquery.easing.min.js')}}"></script>

<!-- Page level plugin JavaScript-->
 <script src="{{asset('BackEnd1/vendor/chart.js/Chart.min.js')}}"></script>
<script src="{{asset('BackEnd1/vendor/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('BackEnd1/vendor/datatables/dataTables.bootstrap4.js')}}"></script>

<!-- Custom scripts for all pages-->
<script src="{{asset('BackEnd1/js/sb-admin.min.js')}}"></script>

<!-- Demo scripts for this page-->
<script src="{{asset('BackEnd1/js/demo/datatables-demo.js')}}"></script>
<!-- <script src="{{asset('BackEnd1/js/demo/chart-area-demo.j')}}s"></script> -->

<!-- csv upload script -->
<script>
    var token = "<?= csrf_token() ?>";
    var BASE_URL = "{{ url('/') }}";
</script>
<script type="text/javascript" src="{{ url('/BackEnd1/js/nikola/csv-upload.js') }}"></script>
<script type="text/javascript" src="{{ url('/BackEnd1/js/nikola/shim.min.js') }}"></script>
<script type="text/javascript" src="{{ url('/BackEnd1/js/nikola/xlsx.core.min.js') }}"></script>
<script type="text/javascript" src="{{ url('/BackEnd1/js/nikola/modal-loading.js') }}"></script>
<!-- Role scripts -->
<script type="text/javascript" src="{{ url('/BackEnd1/js/nikola/role.js') }}"></script>
