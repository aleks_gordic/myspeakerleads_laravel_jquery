<ul class="sidebar navbar-nav">
    <li class="nav-item {{$title=='Dashboard'?'active':''}}">
        <a class="nav-link {{$title=='Dashboard'?'active':''}}" href="{{route('backEnd.index')}}">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span>
        </a>
    </li>
    <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-fw fa-folder"></i>
            <span>Leads</span>
        </a>
        <div class="dropdown-menu" aria-labelledby="pagesDropdown">

            <a class="dropdown-item {{$title=='Leads'?'active':''}}" href="{{route('backEnd.leads')}}">Leads</a>
            <a class="dropdown-item {{$title=='CSV'?'active':''}}" href="{{route('backEnd.csv-upload')}}">Upload Leads</a>
            <a class="dropdown-item {{$title=='Dashboard'?'active':''}}" href="{{route('lead.graph')}}">Lead Graph</a>

            <a class="dropdown-item {{$title=='Dashboard'?'active':''}}" href="{{route('lead.assign')}}">Assign Leads</a>

            <a class="dropdown-item {{$title=='Dashboard'?'active':''}}" href="{{route('lead.update')}}">Update Lead</a>

            <a class="dropdown-item {{$title=='Dashboard'?'active':''}}" href="{{route('lead.add')}}">Add Lead</a>

            <a class="dropdown-item {{$title=='Dashboard'?'active':''}}" href="{{route('lead.settings')}}">Lead Settings Page</a>

        </div>
    </li>

    <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-fw fa-folder"></i>
            <span>Role</span>
        </a>
        <div class="dropdown-menu" aria-labelledby="pagesDropdown">

            <a class="dropdown-item {{$title=='Dashboard'?'active':''}}" href="{{route('role.index')}}">Users</a>
            <a class="dropdown-item {{$title=='Dashboard'?'active':''}}" href="{{route('role.permission')}}">All Permissions</a>

            <a class="dropdown-item {{$title=='Dashboard'?'active':''}}" href="{{route('role.permissionCreate')}}">Create Permissions</a>
            <a class="dropdown-item {{$title=='Dashboard'?'active':''}}" href="{{route('role.role')}}">All Roles</a>

            <a class="dropdown-item {{$title=='Dashboard'?'active':''}}" href="{{route('role.create')}}">Role Create</a>

            <a class="dropdown-item {{$title=='Dashboard'?'active':''}}" href="{{route('role.setPermissionTorole')}}">Assign Permission</a>

            <a class="dropdown-item {{$title=='Dashboard'?'active':''}}" href="{{route('role.assignRoleToUser')}}">Assign Role to User</a>

            <a class="dropdown-item {{$title=='Dashboard'?'active':''}}" href="{{route('role.roleHasPermission')}}">Role has Permissions</a>

        </div>
    </li>




    <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-fw fa-folder"></i>
            <span>Researcher</span>
        </a>
        <div class="dropdown-menu" aria-labelledby="pagesDropdown">

            <a class="dropdown-item {{$title=='Dashboard'?'active':''}}" href="{{route('backend.researcher')}}">Researcher</a>

            <a class="dropdown-item {{$title=='Dashboard'?'active':''}}" href="{{route('backend.addResearcher')}}">Add Researcher</a>

            <a class="dropdown-item {{$title=='Dashboard'?'active':''}}" href="{{route('backend.manageResearcher')}}">Manage Researcher</a>





        </div>
    </li>





    <li class="nav-item {{$title=='Customer'?'active':''}}">
        <a class="nav-link" href="{{route('backend.customer')}}">
            <i class="fas fa-users"></i>
            <span>Customer</span></a>

    </li>
    <li class="nav-item {{$title=='Manager'?'active':''}}">
        <a class="nav-link" href="{{route('backend.manager')}}">
            <i class="far fa-user"></i>
            <span>Manager</span></a>
    </li>


</ul>