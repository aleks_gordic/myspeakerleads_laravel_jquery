@extends('BackEnd.layouts.master')
@section('title','Edit Email')
@section('content')

<div class="col-lg-6 offset-3">
    <h1 class="text-center">Edit Leads Email Address</h1>
<br>
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>
                        @if($error =='The lead email has already been taken.')
                            Please email info@myspeakerleads.com for assistance.
                        @else
                        {{ $error }}</li>
                        @endif
                @endforeach
            </ul>
        </div>
    @endif

    {!! Form::open(['class'=>'form-group', 'method'=>'post', 'route'=>'customer.emailUpdate']) !!}
    {!! Form::label('lead_email','Enter the email address that you want your weekly leads email sent to.  Only this email address will receive the leads file. Please ensure that you enter this email address correctly.') !!}
    {!! Form::text('lead_email', $email , ['class'=>'form-control']) !!}
    <br>
    {!! Form::button('Update', ['class'=>'btn btn-warning','type'=>'submit']) !!}
    {!! Form::close() !!}

</div>

@endsection
