@extends('BackEnd.layouts.master')
@section('title','Customer Page')
@section('content')

    <div class="card">
        <div class="card-header">
            <span>Customer Page</span>
        </div>

        <div class="card-body">
            @if(session()->has('msg'))
                <div class="alert alert-success text-center">
                    {{session('msg')}}
                </div>
            @endif

            <button class="btn btn-success">Resend these weeks leads</button>
            <a href="{{route('customer.emailEdit')}}"><button  class="btn btn-info">Update Leads Email Address</button></a>
            <button  class="btn btn-warning">Update Billing Email Address</button>
            <button  class="btn btn-secondary">Update payment </button>
        </div>
    </div>

    @endsection
