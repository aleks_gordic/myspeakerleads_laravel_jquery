@extends('BackEnd.layouts.master')
@section('title','Add Researcher')
@section('content')

    <div class="row">
        <div class="col-lg-6 offset-3">
            <h1 class="text-center">Add Researcher</h1>
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            {!! Form::open(['class'=>'form-group', 'method'=>'post', 'route'=>'backend.storeResearcher']) !!}
            {!! Form::label('name', 'Insert Name') !!}
            {!! Form::text('name', null ,['class'=>'form-control', 'placeholder'=>'Insert Researcher Full Name']) !!}
            <br>
            {!! Form::label('email', 'Insert Email') !!}
            {!! Form::text('email', null ,['class'=>'form-control', 'placeholder'=>'Insert Researcher Valid Email']) !!}
            <br>
            {!! Form::label('password', 'Insert Password') !!} <small class="text-danger"> (Password should be at least  8 character length)</small>
            {!! Form::password('password' ,['class'=>'form-control','placeholder'=>'Insert Password', 'id'=>'password']) !!}
            <p class="btn btn-sm btn-warning" id="show_password" style="cursor: pointer">Show Password</p>

            <br>
            <br>
            {!! Form::button('Add Researcher', ['class'=>'btn btn-success','type','submit']) !!}
            {!! Form::close() !!}

        </div>
    </div>

<script>
    $(document).ready(function(){
        $('#show_password').click(function(){
            if('password' == $('#password').attr('type')){
                $('#password').prop('type', 'text');
            }else{
                $('#password').prop('type', 'password');
            }
        });
    });
</script>

@endsection