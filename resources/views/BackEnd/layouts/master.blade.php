<!DOCTYPE html>
<html lang="en">

<head>
    @include('BackEnd.includes.head')
    @include('BackEnd.includes.scripts')
</head>

<body id="page-top">

<nav class="navbar navbar-expand navbar-dark bg-dark static-top">
{{--    top bar --}}
    @include('BackEnd.includes.header')
</nav>

<div id="wrapper">

    <!-- Sidebar -->
    @include('BackEnd.includes.sidebar')

    <div id="content-wrapper">

        <div class="container-fluid">

            <!-- Breadcrumbs-->
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{route('backEnd.index')}}">Dashboard</a>
                </li>
                <li class="breadcrumb-item active">@yield('title')</li>
            </ol>

            <!-- Icon Cards-->
            @yield('content')



        </div>
        <!-- /.container-fluid -->

        <!-- Sticky Footer -->
      @include('BackEnd.includes.footer')

    </div>
    <!-- /.content-wrapper -->

</div>
<!-- /#wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>

<!-- Logout Modal-->
{{--<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">--}}
{{--    <div class="modal-dialog" role="document">--}}
{{--        <div class="modal-content">--}}
{{--            <div class="modal-header">--}}
{{--                <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>--}}
{{--                <button class="close" type="button" data-dismiss="modal" aria-label="Close">--}}
{{--                    <span aria-hidden="true">×</span>--}}
{{--                </button>--}}
{{--            </div>--}}
{{--            <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>--}}
{{--            <div class="modal-footer">--}}
{{--                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>--}}
{{--                <a class="btn btn-primary" href="login.html">Logout</a>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</div>--}}

<!-- Bootstrap core JavaScript-->



</body>

</html>

