@extends('BackEnd.layouts.master')
@section('title','Selected Fields For Email')

@section('content')
    <div class="card mb-3">
        <div class="card-header">
            <i class="fas fa-table"></i>
            Leads Table</div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th>SL</th>
                        @foreach($showFields as $showField)
                            <th>
                                {{ ucwords(str_replace('_',' ',$showField))}}
                            </th>
                        @endforeach

                        <th>Action</th>
                    </tr>

                    </thead>
                    <tfoot>
                    <tr>
                        <th>SL</th>
                        @foreach($showFields as $showField)
                            <th>
                                {{ ucwords(str_replace('_',' ',$showField))}}
                            </th>
                        @endforeach
                        <th>Action</th>
                    </tr>
                    </tfoot>

                    </thead>

                    <tbody>
                    @php
                        $sl = 1;
                    @endphp
                    @foreach($leads as $lead)

                        <tr>
                            <td>{{$sl++}}</td>
                            @foreach($showFields as $showField)
                                <td>{{$lead->$showField}}</td>
                            @endforeach
                            <td>
                                <button class="btn btn-success" id="active_permission" user-id="{{$lead->id}}">Action</button>
                            </td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>
            </div>
        </div>
        <div class="card-footer small text-muted">Updated at {{$lastUpdate->updated_at->diffForHumans()}}</div>
    </div>
@endsection
