@extends('BackEnd.layouts.master')
@section('title','CSV Upload')

@section('content')
<div class="upload-btn-wrapper">
    <label>Select CSV</label>
    <input id="csv_file_select" type="file" accept=".csv, .xlsx, text/csv"/>
</div>
<div class="upload-btn-wrapper">
    <a href="#" id="download_csv">Download Sample CSV</a>
    <input type="submit" class="btn btn-info" id="csv_file_upload" value="Submit">
</div>

<div class="csv_table">
</div>
@endsection
