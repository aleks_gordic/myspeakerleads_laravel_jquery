@extends('BackEnd.layouts.master')
@section('title','Lead Graph')
@section('content')

    <div class="card">
        <div class="card-header">
            <h3>Lead Graph</h3>
        </div>
        <div class="card-body">
            <span class="btn btn-warning">Total Leads : 10</span>
            <span class="btn btn-success">Active Leads : 7</span>
            <span class="btn btn-danger">Inactive Leads : 3</span>
        </div>
    </div>
    <div class="col-lg-4 offset-4">
        <canvas id="myChart" width="200px" height="200px"></canvas>
    </div>

    <script>
        var ctx = document.getElementById('myChart').getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: ['Total', 'Active', 'Inactive'],
                datasets: [{
                    label: 'Total Leads',
                    data: [10, 0 , 0],
                    backgroundColor: "rgba(255,242,0,0.5)"
                },
                    {
                    label: 'Active Leads',
                    data: [0,7,0],
                    backgroundColor: "rgba(18,183,74,0.5)"
                }, {
                    label: 'Inactive Leads',
                    data: [0,0,3],
                    backgroundColor: "rgba(255,0,6,0.5)"
                }
                ]
            }
        });
    </script>



    @endsection