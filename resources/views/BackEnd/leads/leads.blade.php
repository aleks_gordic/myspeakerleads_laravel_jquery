@extends('BackEnd.layouts.master')
@section('title','CSV Upload')

@section('content')
<div class="card mb-3">
    <div class="card-header">
        <i class="fas fa-table"></i>
        Leads Table</div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Type of Lead</th>
                    <th>Email</th>
                    <th>Name</th>
                    <th>Phone Number</th>
                    <th>Update Date</th>
                    <th>Expiry Date</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tfoot>
                <tr>
                    <th>ID</th>
                    <th>Type of Lead</th>
                    <th>Email</th>
                    <th>Name</th>
                    <th>Phone Number</th>
                    <th>Update Date</th>
                    <th>Expiry Date</th>
                    <th>Action</th>
                </tr>
                </tfoot>
                <tbody>
                @foreach($leads as $lead)
                <tr>
                    <td>{{$lead->id}}</td>
                    <td>{{$lead->lead_type}}</td>
                    <td>{{$lead->email}}</td>
                    <td>{{$lead->first_name}} {{$lead->last_name}}</td>
                    <td>{{$lead->phone_number}}</td>
                    <td>{{$lead->update_date}}</td>
                    <td>{{$lead->expiry_date}}</td>
                    <td>
                        <button class="btn btn-success" id="active_permission" user-id="{{$lead->id}}">Action</button>
                    </td>
                </tr>
                @endforeach

                </tbody>
            </table>
        </div>
    </div>
    <div class="card-footer small text-muted">
        @if ($lastUpdate != null)
            Updated at {{$lastUpdate->updated_at->diffForHumans()}}
        @else
            No uploaded data
        @endif
    </div>
</div>
@endsection
