@extends('BackEnd.layouts.master')
@section('title','Lead Settings')
@section('content')
    <style>
        .table-striped {
            margin: auto;
            width: 50% !important;

        }
        .table-striped ,tr, td {
            padding: 5px;
        }
    </style>

    <h1 class="text-center">Lead Settings Page</h1>
    <br> <br>
<div class="row">
    <div class="col-lg-6">
        <h3>Check/Uncheck Fields Settings</h3>
        <br>

        <table class="table-striped">
            @foreach($fields as $field)
            <tr>
              <td>{{$field->column_name}}</td>
                <td>
                    @if($field->status ==1)

                    <a href="{{route('lead.updateSettings',$field->id)}}" ><button class="btn btn-outline-danger">Hide</button></a>
                    @else
                        <a href="{{route('lead.updateSettings',$field->id)}}" >  <button class="btn btn-outline-success">Show</button></a>
                    @endif
                </td>
            </tr>
           @endforeach



        </table>
    </div>

    <div class="col-lg-4 offset-1">
        <h3>Current Database Fields</h3>
        <br>
        @foreach($columns as $id =>$column)
     {{$column}}  <br>
        @endforeach
<hr>
<h5>Insert Column name to Settings</h5>
        {!! Form::open(['method'=>'post','route'=>'lead.columnSave']) !!}
{{--        {!! Form::select('column_name', $columns,2 ,['class'=>'form-control']) !!}--}}

        <select name="column_name" class="form-control">
            @php
                 // $skip =['id','lead_type'];
                    @endphp
            <option value="">Choose One</option>
            @foreach($columns as $id =>$column)
                @if(in_array($column,$skip))
                    @continue
                    @endif
            <option value="{{$column}}">{{$column}}</option>
                @endforeach
        </select>
        <br>
        {!! Form::button('Save', ['class'=>'btn btn-success', 'type'=>'submit']) !!}
        {!! Form::close() !!}
    </div>
</div>
@endsection