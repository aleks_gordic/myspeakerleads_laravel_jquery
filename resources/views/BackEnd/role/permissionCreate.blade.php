@extends('BackEnd.layouts.master')
@section('title','Permission Create')
@section('content')

    <div class="row">
        <div class="col-lg-6 offset-3">
            <h1 class="text-center">Permission Create</h1>
            <br>
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if(session()->has('msg'))
                <div class="alert alert-success text-center">
                    {{session('msg')}}
                </div>
            @endif

            {!! Form::open(['class'=>'form-group','method'=>'post','route'=>'role.permissionStore']) !!}
            {!! Form::text('name', null, ['placeholder'=>'Insert Permission Name','class'=>'form-control']) !!} <br>
            {!! Form::button('Save', ['type'=>'submit', 'class'=>'btn btn-success']) !!}
            {!! Form::close() !!}
        </div>
    </div>
    @endsection