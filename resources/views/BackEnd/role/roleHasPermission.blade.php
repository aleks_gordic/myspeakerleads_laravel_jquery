@extends('BackEnd.layouts.master')
@section('title','Role Has Permission')
@section('content')
<h1 class="text-center">Role Has Permission</h1>
@if(session()->has('msg'))
    <div class="alert alert-success text-center">
        {{session('msg')}}
    </div>
@endif
@foreach($roles as $role)
<style>
   .btn-secondary{
       width: 300px;
       margin-top: 5px;
       text-align: center;
   }
    .list-item{
        text-align: center;
    }
</style>

    <ul style="list-style: none">
        <li><span class="btn-block btn btn-success">{{$role->name}}</span>
            <ul style="list-style: none">
                @foreach($role->permissions as $permission)
                <li class="list-item">
                 <span class="btn btn-secondary"> {{$permission->name}}</span>
                    <a href="{{route('role.permissionRevoke',[$role->id,$permission->id])}}">
                        <button class="btn btn-danger" onclick="return confirm('Are You Sure? Want To Remove Permission?')">Revoke</button>
                    </a>


                </li>
                @endforeach
            </ul>
        </li>
    </ul>

    @endforeach

    @endsection