@extends('BackEnd.layouts.master')
@section('title','All Permissions')
@section('content')
    <div class="row">
        <div class="col-lg-6 offset-3">
            <style>
                .table-striped{
                    margin: 0 auto;
                }
                .table-striped, th, td{
                    padding: 10px;
                }
            </style>
            <h1 class="text-center">All Permissions</h1>
            <br>
            @if(session()->has('msg'))
                <div class="alert alert-success text-center">
                    {{session('msg')}}
                </div>
            @endif
            <table class="table-striped">
                @php
                    $sl=1;
                @endphp
                <tr>
                    <th>SL</th>
                    <th>Permission Name</th>
                    <th>Action</th>
                </tr>
                @foreach($permissions as $permission)
                    <tr>
                        <td>{{$sl++}}</td>
                        <td>{{$permission->name}}</td>
                        <td>
                            <div style="display: inline-flex">
                                <a href="{{route('permission.edit',$permission->id)}}"><button class="btn btn-warning">Edit</button></a>
                            </div>
                            <div style="display: inline-flex">
                                {{Form::open(['class'=>'form-inline','method'=>'delete', 'route'=>array('permission.delete',$permission->id)])}}
                                {{Form::button('Delete',['type'=>'submit', 'class'=>'btn btn-danger','onclick'=>"return confirm('Are you Sure? You want to delete?')"])}}
                                {{Form::close()}}
                            </div>
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
@endsection