@extends('BackEnd.layouts.master')
@section('title','Assign Role To User')
@section('content')
    <style>
        .form-inline{
            margin: 0 auto;
        }
    </style>
    <div class="row">
        <div class="col-lg-8 offset-2">
            <h1>Assign Role To User</h1>
            <br> <br>
            @if(session()->has('msg'))
                <div class="alert alert-success text-center">
                    {{session('msg')}}
                </div>
            @endif

            {!! Form::open(['class'=>'form-inline', 'route'=>'role.assignRoleToUserStore','method'=>'post']) !!}
            {!! Form::select('user_id', $users, 1, ['class'=>'form-control']) !!}
            {!! Form::select('role_id', $roles, 1, ['class'=>'form-control']) !!}
            {!! Form::button('Assing', ['type'=>'submit', 'class'=>'btn btn-success','onClick'=>'return confirm("Are you sure? Want to Assign Permission?")']) !!}
            {!! Form::close() !!}

        </div>
    </div>
    </div>

@endsection