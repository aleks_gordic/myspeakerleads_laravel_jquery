@extends('BackEnd.layouts.master')
@section('title','Role Edit')
@section('content')

    <div class="row">
        <div class="col-lg-6 offset-3">
            <h1 class="text-center">Role Edit</h1>
            <br>
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if(session()->has('msg'))
                <div class="alert alert-success text-center">
                    {{session('msg')}}
                </div>
            @endif

            {!! Form::open(['class'=>'form-group','method'=>'put','route'=>array('role.update', $role->id)]) !!}
            {!! Form::text('name', $role->name, ['placeholder'=>'Insert Role Name','class'=>'form-control']) !!} <br>
            {!! Form::button('Update', ['type'=>'submit', 'class'=>'btn btn-success']) !!}
            {!! Form::close() !!}
        </div>
    </div>
@endsection
