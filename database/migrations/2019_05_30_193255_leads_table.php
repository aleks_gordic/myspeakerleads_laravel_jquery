<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class LeadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leads', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('lead_type', 255)->nullable();
            $table->string('email', 255)->nullable();
            $table->string('first_name', 255)->nullable();
            $table->string('last_name', 255)->nullable();
            $table->string('phone_number', 255)->nullable();
            $table->string('job_title', 255)->nullable();
            $table->string('company_name', 255)->nullable();
            $table->string('industry', 255)->nullable();
            $table->text('website', 'url')->nullable();
            $table->string('previous_event_url', 255)->nullable();
            $table->string('previous_event_speaker_url', 255)->nullable();
            $table->string('previous_event_date', 255)->nullable();
            $table->string('next_event_url', 255)->nullable();
            $table->string('next_event_name', 255)->nullable();
            $table->string('next_event_date', 255)->nullable();
            $table->string('call_speaker_url', 255)->nullable();
            $table->string('call_speaker_deadline', 255)->nullable();
            $table->string('compensation_pay', 255)->nullable();
            $table->string('notes_event', 255)->nullable();
            $table->string('expiry_date', 255)->nullable();
            $table->string('time_note', 255)->nullable();
            $table->string('update_date', 255)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
