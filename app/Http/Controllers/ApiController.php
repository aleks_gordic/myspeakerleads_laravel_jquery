<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Leads;

class ApiController extends Controller
{
    public function upload_csv(Request $request) {
        if ($request->file('file')->isValid()){
            $file_name = $_FILES['file']['name'];
            $file_tmp = $_FILES['file']['tmp_name'];
            $dir = dirname(dirname(dirname(dirname(__FILE__))));
            $uploadfile = $dir . '/public/assets/uploads/documents/'.$file_name;            
             
            if (!file_exists($uploadfile)) {
                move_uploaded_file($file_tmp, $uploadfile);
                $file = fopen($uploadfile,"r");
                $count = 0;
                while(! feof($file)) {
                    $line = fgetcsv($file);
                    $line_ = trim(fgets($file)); // get line as a string
                    if ($count > 0 && !empty($line_)) { // ignore blank lines and header
                        $lead = [
                            'lead_type' => $line[0]?$line[0]:NULL,
                            'email' => $line[1]?$line[1]:NULL,
                            'first_name' => $line[2]?$line[2]:NULL,
                            'last_name' => $line[3]?$line[3]:NULL,
                            'phone_number' => $line[4]?$line[4]:NULL,
                            'job_title' => $line[5]?$line[5]:NULL,
                            'company_name' => $line[6]?$line[6]:NULL,
                            'industry' => $line[7]?$line[7]:NULL,
                            'website' => $line[8]?$line[8]:NULL,
                            'previous_event_url' => $line[9]?$line[9]:NULL,
                            'previous_event_speaker_url' => $line[10]?$line[10]:NULL,
                            'previous_event_date' => $line[11]?$line[11]:NULL,
                            'next_event_url' => $line[12]?$line[12]:NULL,
                            'next_event_name' => $line[13]?$line[13]:NULL,
                            'next_event_date' => $line[14]?$line[14]:NULL,
                            'call_speaker_url' => $line[15]?$line[15]:NULL,
                            'call_speaker_deadline' => $line[16]?$line[16]:NULL,
                            'compensation_pay' => $line[17]?$line[17]:NULL,
                            'notes_event' => $line[18]?$line[18]:NULL,
                            'expiry_date' => $line[19]?$line[19]:NULL,
                            'time_note' => $line[20]?$line[20]:NULL,
                            'update_date' => date("Y-m-d")
                        ];
                        if (!Leads::where('email', $lead['email'])->exists()) {
                            Leads::create($lead);
                        }
                    }
                    $count ++;
                }
                fclose($file);
                return ['result' => 'Uploaded successfully!'];
            } else {
                return ['result' => "File name is same. This file is already uploaded!"];
            }
        } else {
            return ["result" => "No csv file attached"];
        }
    }

    public function download_csv() {        
        $csv_key_values = ['Type of Lead', 'Email', 'First name', 'Last name', 'Phone number', 'Job title', 'Company name', 'Industry', 'Website URL Link', 'Previous event URL link', 'Previous event speaker(s) URL link', 'Previous Event Date', 'Next Event URL Link', 'Next Event name', 'Next Event Date', 'Call For Speakers URL Link', 'Call For Speakers deadline', 'Compensation or Pay?', 'Notes Regarding Event', 'Expiry Date', 'Time Note'];
        
        // // save exported csv file into server
        $dir = dirname(dirname(dirname(dirname(__FILE__))));
        $downloadfile = $dir . '/public/assets/downloads/documents/Sample_CSV.csv';       

        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename=Sample_CSV.csv');

        // // create a file pointer connected to the output stream        
        $output = fopen($downloadfile, 'w');

        // output the column headings
        fputcsv($output, $csv_key_values);        

        $lead = Leads::where('email', '!=', NULL)->first();    
        $arr = [$lead->lead_type, $lead->email, $lead->first_name, $lead->last_name, $lead->phone_number, $lead->job_title, $lead->company_name, $lead->industry, $lead->website, $lead->previous_event_url, $lead->previous_event_speaker_url, $lead->previous_event_date, $lead->next_event_url, $lead->next_event_name, $lead->next_event_date, $lead->call_speaker_url, $lead->call_speaker_deadline, $lead->compensation_pay, $lead->notes_event, $lead->expiry_date, $lead->time_note];
        fputcsv($output, $arr);        
        
        fclose($output);

        return ['result' => '/assets/downloads/documents/Sample_CSV.csv'];
    }
}
