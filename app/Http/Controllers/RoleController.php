<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::with('roles')->orderBy('created_at', 'desc')->get();
       // dd($users);
        $lastUpadate= User::orderBy('updated_at', 'desc')->first();
        $title = 'Role';
        return view('BackEnd/role/index', compact('users','lastUpadate', 'title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function roleHasPermission()
    {
        $roles =Role::with('permissions')->orderBy('id', 'desc')->get();
        $title = 'Permission';
        return view('BackEnd/role/roleHasPermission', compact('roles','title'));
    }

    public function permissionRevoke($role_id, $permission_id)
    {
        $permission =Permission::findById($permission_id);
        $role =Role::findById($role_id);
        $permission->removeRole($role);
        session()->flash('msg', 'Permission Revoked');
        return redirect()->back();

    }

    public function assignRoleToUser()
    {
        $users = User::pluck('email','id');
        $roles =Role::pluck('name','id');
        $title= 'Assign Role To User';
        return view('BackEnd/role/assignRoleToUser', compact('title','roles','users'));

    }

    public function assignRoleToUserStore(Request $request)
    {
       $user = User::findOrFail($request->input('user_id'));
       $role= Role::findById($request->input('role_id'));
        $roles = $user->getRoleNames();
        if($roles->isEmpty()){
            $user->assignRole($role);
        }
        else{
            $user->removeRole($roles[0]);
            $user->assignRole($role);
        }



        session()->flash('msg','Role Assigned Successfully');
        return redirect()->back();
    }

    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    public function permissionCreate()
    {
        $title = "Permission Create";
        return view('BackEnd/role/permissionCreate', compact('title'));
    }

    public function permissionStore(Request $request)
    {
        $data = $request->validate([
           'name'=>'required|unique:permissions',
        ]);
        Permission::create($data);
        session()->flash('msg','Permission Added Successfully');
        return redirect()->back();
    }



    public function permission()
    {
        $permissions = Permission::all();
        $title= 'Permission';
        return view('BackEnd/role/permission', compact('title','permissions'));
    }

    public function permissionEdit($id)
    {
        $permission= Permission::findById($id);
        $title= 'Permission Edit';
        return view('BackEnd/role/permissionEdit', compact('title','permission'));
    }


    public function permissionUpdate(Request $request, $id)
    {
        $permission = Permission::findById($id);

        $data= $request->validate([

            'name'=>'required|unique:permissions'
        ]);
        $permission->update($data);

        session()->flash('msg','Permission Updated Successfully');
        return redirect()->route('role.permission');
    }


    public function permissionDelete($id)
    {
        $permission = Permission::findById($id);
        $permission->delete();
        session()->flash('msg','Permission Deleted Successfully');
        return redirect()->back();
    }

    public function role()
    {
        $roles = Role::all();
        $title= 'Role';
        return view('BackEnd/role/roles', compact('title','roles'));
    }
    public function roleCreate()
    {
        $title= 'Role Create';
        return view('BackEnd/role/roleCreate', compact('title'));

    }


    public function roleStore(Request $request)
    {
        $data = $request->validate([
            'name'=>'required|unique:roles',
        ]);
        Role::create($data);
        session()->flash('msg','Role Added Successfully');
        return redirect()->back();
    }
    public  function  roleEdit($id)
    {
        $role= Role::findById($id);
        $title= "Role Edit";
        return view('BackEnd/role/roleEdit', compact('title', 'role'));
    }

    public  function roleDelete($id)
    {
        $role= Role::findById($id);
        $role->delete();

        session()->flash('msg','Role Deleted Successfully');
        return redirect()->back();
    }

    public  function  roleUpdate(Request $request, $id)
    {
        $role = Role::findById($id);

        $data= $request->validate([

            'name'=>'required|unique:roles'
        ]);
        $role->update($data);

        session()->flash('msg','Role Updated Successfully');
        return redirect()->route('role.role');
    }

    public function setPermissionTorole()
    {
        $title='Assign Permission';
        $roles = Role::pluck('name','id');
        $permissions = Permission::pluck('name','id');
        return view('Backend/role/rolesetPermissionTorole', compact('title','roles','permissions'));
    }
    public function setPermissionToroleStore(Request $request)
    {
        $role = Role::findById($request->input('role_id'));
        $permission = Permission::findById($request->input('permission_id'));
        $role->givePermissionTo($permission->name);
        session()->flash('msg','Permission Assigned Successfully');
        return redirect()->back();
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
