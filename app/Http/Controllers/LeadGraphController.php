<?php

namespace App\Http\Controllers;

use App\Leads;
use App\Settings;
use Illuminate\Http\Request;

class LeadGraphController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function leadGraph()
    {
        $title = "Admin Graph";
        return view('BackEnd/leads/index', compact('title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function assignLeads()
    {
        $title= "Assign Leads";
        return view('BackEnd/leads/assignLeads', compact('title'));
    }
    public function updateLeads()
    {
        $title= "Update Leads";
        return view('BackEnd/leads/updateLeads', compact('title'));
    }
    public function addLeads()
    {
        $title= "Add Leads";
        return view('BackEnd/leads/addLeads', compact('title'));
    }
    public function settingsLeads()
    {
        $leadsTable = new Leads;
        $columns=$leadsTable->getTableColumns();
        $fields = Settings::all();
        $skip = $fields->pluck('column_name')->toArray();

        $title= "Settings Leads";
        return view('BackEnd/leads/settingsLeads', compact('title','columns','fields','skip'));
    }
    public  function columnSave(Request $request)
    {
        $data  = $request->validate([
            'column_name' => 'required|unique:settings'
        ]);
        $data['status']= 0;
        Settings::create($data);
        return redirect()->back();
    }

    public function updateSettingsLeads($id)
    {
        $column = Settings::findOrFail($id);
        if ($column->status == 0){
            $data['status']=1;
        }
        else{
            $data['status']=0;
        }
        $column->update($data);

        return redirect()->back();
    }
    public function leadsSelect() {
        $leads = Leads::get();

        $showFields = Settings::where('status', 1)->pluck('column_name')->toArray();

        $lastUpdate= Leads::orderBy('updated_at', 'desc')->first();
        return view('BackEnd/leads/leadsSelect', compact('leads', 'lastUpdate','showFields'), ["title" => "Leads"]);
    }

    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
