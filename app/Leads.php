<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Leads extends Model
{
          /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'leads';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * @var array
     */

    protected $fillable = ['id', 'lead_type', 'email', 'first_name', 'last_name', 'phone_number', 'job_title', 'company_name', 'industry', 'website', 'previous_event_url', 'previous_event_speaker_url', 'previous_event_date', 'next_event_url', 'next_event_name', 'next_event_date', 'call_speaker_url', 'call_speaker_deadline', 'compensation_pay', 'notes_event', 'expiry_date', 'time_note', 'update_date'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getTableColumns() {
        return $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
    }
}
