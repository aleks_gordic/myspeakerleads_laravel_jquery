<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});




Route::group(['middleware' => 'auth', 'prefix'=>'dashboard'], function(){


    // dashboard index
    Route::get('/', 'BackEndController@index')->name('backEnd.index');
//<<<<<<< HEAD

    Route::get('/role', 'RoleController@index')->name('role.index');

    Route::get('/permission_create', 'RoleController@permissionCreate')->name('role.permissionCreate');
    Route::post('/permission_create', 'RoleController@permissionStore')->name('role.permissionStore');
    Route::get('/permission', 'RoleController@permission')->name('role.permission');

    Route::get('/permission/{id}/edit', 'RoleController@permissionEdit')->name('permission.edit');

    Route::put('/permission/{id}', 'RoleController@permissionUpdate')->name('permission.update');

    Route::delete('/permission/{id}', 'RoleController@permissionDelete')->name('permission.delete');


    Route::get('/all_role', 'RoleController@role')->name('role.role');
    Route::get('/role_create', 'RoleController@roleCreate')->name('role.create');
    Route::post('/role_create', 'RoleController@roleStore')->name('role.store');
    Route::get('/role_edit/{id}/edit', 'RoleController@roleEdit')->name('role.edit');
    Route::put('/role_edit/{id}', 'RoleController@roleUpdate')->name('role.update');
    Route::delete('/role_edit/{id}', 'RoleController@roleDelete')->name('role.delete');

    Route::get('/role_has_permission', 'RoleController@roleHasPermission')->name('role.roleHasPermission');

    Route::get('/assign_role_to_user', 'RoleController@assignRoleToUser')->name('role.assignRoleToUser');
    Route::post('/assign_role_to_user', 'RoleController@assignRoleToUserStore')->name('role.assignRoleToUserStore');



    Route::get('/permission_revoke/{role_id}/{permission_id}', 'RoleController@permissionRevoke')->name('role.permissionRevoke');

    Route::get('/set_permission_to_role', 'RoleController@setPermissionTorole')->name('role.setPermissionTorole');
    Route::post('/set_permission_to_role', 'RoleController@setPermissionToroleStore')->name('role.setPermissionToroleStore');

    Route::get('/lead_graph', 'LeadGraphController@leadGraph')->name('lead.graph');
    Route::get('/assign_leads', 'LeadGraphController@assignLeads')->name('lead.assign');
    Route::get('/update_leads', 'LeadGraphController@updateLeads')->name('lead.update');
    Route::get('/add_leads', 'LeadGraphController@addLeads')->name('lead.add');

    Route::get('/leads_select', 'LeadGraphController@leadsSelect')->name('lead.select');

    Route::get('/settings_leads', 'LeadGraphController@settingsLeads')->name('lead.settings');
    Route::get('/settings_leads/{id}', 'LeadGraphController@updateSettingsLeads')->name('lead.updateSettings');
    Route::post('/settings_columnSave', 'LeadGraphController@columnSave')->name('lead.columnSave');


    Route::get('/customer', 'CustomerController@customer')->name('backend.customer');
    Route::get('/leads_email_update', 'CustomerController@emailEdit')->name('customer.emailEdit');
    Route::post('/leads_email_update', 'CustomerController@emailUpdate')->name('customer.emailUpdate');


    Route::get('/manager', 'ManagerController@manager')->name('backend.manager');



    Route::get('/researcher', 'ResearcherController@researcher')->name('backend.researcher');
    Route::get('/add_researcher', 'ResearcherController@addResearchers')->name('backend.addResearcher');
    Route::post('/add_researcher', 'ResearcherController@storeResearchers')->name('backend.storeResearcher');
    Route::get('/manage_researcher', 'ResearcherController@manageResearchers')->name('backend.manageResearcher');

//=======
    Route::get('/leads', 'BackEndController@leads')->name('backEnd.leads');
    Route::get('/csv-upload', 'BackEndController@csv_upload')->name('backEnd.csv-upload');
    Route::post('/upload_csv', 'ApiController@upload_csv');
    Route::get('/download_csv', 'ApiController@download_csv');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
