#!/bin/bash
#Get servers list
set -f
string=$DEPLOY_SERVER
array=(${string//,/ })
#Iterate servers for deploy and pull last commit
for i in "${!array[@]}" 
do    
      echo "Deploying project on server ${array[i]} ..."    
      ssh myspeake@${array[i]} -p18765 "
      cd /home/myspeake/public_html/app
      git reset --hard origin/master
      git pull origin master
      composer install
      composer dumpautoload -o
      /usr/local/bin/php73 artisan config:clear
      /usr/local/bin/php73 artisan key:generate
      /usr/local/bin/php73 artisan config:clear
      /usr/local/bin/php73 artisan config:cache
      /usr/local/bin/php73 artisan migrate
      /usr/local/bin/php73 artisan config:cache"
done