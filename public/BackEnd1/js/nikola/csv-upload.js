$( document ).ready(function() {
    var selected_file = null;
    
    var csvUpload = document.getElementById('csv_file_select');
    if (csvUpload) {
        csvUpload.addEventListener('change', handleFile, false);
    }

    var rABS = true; // true: readAsBinaryString ; false: readAsArrayBuffer
    function handleFile(e) {
        $(".csv_table").removeClass('success_message');
        $(".csv_table").removeClass('error_message');

        var files = e.target.files, f = files[0];
        selected_file = files[0];

        var reader = new FileReader();

        reader.onload = function(e) {
            var data = e.target.result;
            if(!rABS) data = new Uint8Array(data);
            var workbook = XLSX.read(data, {type: rABS ? 'binary' : 'array'});
            
            var first_sheet_name = workbook.SheetNames[0];
            var worksheet = workbook.Sheets[first_sheet_name];
            var data = XLSX.utils.sheet_to_json(worksheet);
            var table = "<div class='card mb-3'><div class='card-header'><i class='fas fa-table'></i>Leads in CSV<span id='total_leads'>Total Leads:";
            table += data.length + "</span></div><div class='card-body'><div class='table-responsive'><table class='table table-bordered' id='dataTable' width='100%' cellspacing='0'>";
            table += "<thead><tr><th>Type of Lead</th><th>Email</th><th>First Name</th><th>Last Name</th><th>Phone Number</th><th>Job Title</th><th>Company Name</th><th>Industry</th><th>Website</th><th>Previous Event URL</th><th>Previous Event Speaker URL</th><th>Previous Event Date</th><th>Next Event URL</th><th>Next Event Name</th><th>Next Event Date</th><th>Call Speaker URL</th><th>Call Speaker Deadline</th><th>Compensation Pay</th><th>Notes Event</th><th>Expiry Date</th><th>Time Note</th></tr></thead><tbody>";
            
            data.forEach(element => {
                table +="<tr><td>";
                table += element['Type of Lead']?element['Type of Lead']:'N/A';
                table += "</td><td>";
                table += element['Email']?element['Email']:'N/A';
                table += "</td><td>";
                table += element['First name']?element['First name']:'N/A';
                table += "</td><td>";
                table += element['Last name']?element['Last name']:'N/A';
                table += "</td><td>";
                table += element['Phone number']?element['Phone number']:'N/A';
                table += "</td><td>";
                table += element['Job title']?element['Job title']:'N/A';
                table += "</td><td>";
                table += element['Company name']?element['Company name']:'N/A';
                table += "</td><td>";
                table += element['Industry']?element['Industry']:'N/A';
                table += "</td><td>";
                table += element['Website URL Link']?element['Website URL Link']:'N/A';
                table += "</td><td>";
                table += element['Previous event URL link']?element['Previous event URL link']:'N/A';
                table += "</td><td>";
                table += element['Previous event speaker(s) URL link']?element['Previous event speaker(s) URL link']:'N/A';
                table += "</td><td>";
                table += element['Previous Event Date']?element['Previous Event Date']:'N/A';
                table += "</td><td>";
                table += element['Next Event URL Link']?element['Next Event URL Link']:'N/A';
                table += "</td><td>";
                table += element['Next Event name']?element['Next Event name']:'N/A';
                table += "</td><td>";
                table += element['Next Event Date']?element['Next Event Date']:'N/A';
                table += "</td><td>";
                table += element['Call For Speakers URL Link']?element['Call For Speakers URL Link']:'N/A';
                table += "</td><td>";
                table += element['Call For Speakers deadline']?element['Call For Speakers deadline']:'N/A';
                table += "</td><td>";
                table += element['Compensation or Pay?']?element['Compensation or Pay?']:'N/A';
                table += "</td><td>";
                table += element['Notes Regarding Event']?element['Notes Regarding Event']:'N/A';
                table += "</td><td>";
                table += element['Expiry Date']?element['Expiry Date']:'N/A';
                table += "</td><td>";
                table += element['Time Note']?element['Time Note']:'N/A';
                table += "</td></tr>";
            });
            table += "</tbody><table></div></div>";
            $(".csv_table").html(table);
        }
        if(rABS) reader.readAsBinaryString(f); else reader.readAsArrayBuffer(f);
    }

    $("#csv_file_upload").click(function () {
        $(".csv_table").removeClass('success_message');
        $(".csv_table").removeClass('error_message');
        var formData = new FormData();
        formData.append('file', selected_file);

        if (selected_file) {
            var loading = new Loading();
            $.ajax({
                url: BASE_URL + '/dashboard/upload_csv',
                type:"POST",
                processData:false,
                contentType: false,
                data: formData,
                beforeSend: function (request) {
                    request.setRequestHeader("X-CSRF-TOKEN", token);
                },
                success: function (e) {
                    loading.out();
                    if (e.result == "Uploaded successfully!") {
                        $(".csv_table").addClass('success_message');
                    } else {
                        $(".csv_table").addClass('error_message');
                    }
                    $(".csv_table").html(e.result);
                }
            });
        } else {
            alert("please select file!");
        }
    });

    $("#download_csv").click(function () {
        var loading = new Loading();
        $.ajax({
            url: BASE_URL + '/dashboard/download_csv',
            type:"GET",
            dataType: 'json',
            data: {'type': 'download csv'},
            beforeSend: function (request) {
                request.setRequestHeader("X-CSRF-TOKEN", token);
            },
            success: function (e) {
                loading.out();
                $(".csv_table").addClass('success_message');
                $(".csv_table").html("Downloaded successfully!");
                window.location.href = BASE_URL + e.result;
            }
        });
    });
});